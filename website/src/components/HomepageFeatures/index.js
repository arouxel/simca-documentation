import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Analyze Scenes',
    Svg: require('@site/static/img/spectrums.svg').default,
    description: (
      <>
        SIMCA provides a set of tools to analyze the spatial and spectral content of a scene.
      </>
    ),
  },
  {
    title: 'Design the Optical System',
    Svg: require('@site/static/img/mask_to_detector.svg').default,
    description: (
      <>
        SIMCA is designed to generate realistic measurements based on ray-tracing. Most of the system parameters are tweakable. 
      </>
    ),
  },
  {
    title: 'Generate Masks',
    Svg: require('@site/static/img/mask_display.svg').default,
    description: (
      <>
        SIMCA can be used to quickly generate and test masks (random, blue-noise, slits) and the corresponding filtering.
      </>
    ),
  },
  {
    title: 'Simulate CASSI measurements',
    Svg: require('@site/static/img/acquisition.svg').default,
    description: (
      <>
        SIMCA generates CASSI-like compressed measurements of the scene.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
