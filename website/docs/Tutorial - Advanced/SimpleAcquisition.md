---
sidebar_position: 1
---

# Running a simple acquisition


This tutorial walks you through the process of running a simple acquisition using the `CassiSystem` class. 

## Setup

First, make sure to import the necessary modules:

```python
from CassiSystem import CassiSystem
from utils.helpers import load_yaml_config
```

Next, load the configuration files:

```python
config_dataset = load_yaml_config("config/dataset.yml")
config_system = load_yaml_config("config/cassi_system.yml")
config_masks = load_yaml_config("config/filtering.yml")
config_acquisition = load_yaml_config("config/acquisition.yml")


```

Then, set the name of the dataset of interest:

```python
dataset_name = "PaviaU"
```

## Initialize the CassiSystem

Initialize the `CassiSystem`. It creates the coordiantes grids of the SLM and of the detector :

```python
cassi_system = CassiSystem(system_config_path="config/cassi_system.yml")
```

## Load the Hyperspectral dataset

Load the hyperspectral dataset :

```python
cassi_system.load_dataset(scene_name, scene_directory)
```

## Generate the SLM Mask

Generate the SLM mask based on the parameters defined in the configuration file :

```python
cassi_system.generate_2D_mask(config_masks)
```

## Propagate the Mask Grid

Propagate the mask grid to the detector plane for each wavelength defined by the cassi system spectral sampling :

```python
cassi_system.propagate_mask_grid()
```

## Generate the Filtering Cube

Generate the filtering cube based on the optical behaviour of the defined cassi system :

```python
cassi_system.generate_filtering_cube()
```

## (Optional) Generate the PSF

Generate the PSF of optical system between the SLM and the detector :

```python
cassi_system.generate_psf(type="Gaussian",radius=100)
```

## Simulate the Acquisition

Simulate the acquisition (with PSF in this case) :

```python
cassi_system.image_acquisition(use_psf=True,chunck_size=50)
```

## Save the Acquisition

Finally, save the acquisition:

```python
cassi_system.save_acquisition(config_masks, config_acquisition)
```

And that's it! You've successfully run an acquisition using the `CassiSystem` class. 
