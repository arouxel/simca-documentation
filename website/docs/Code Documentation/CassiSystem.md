---
sidebar_position: 1
---

# Cassi System (class)

```python
class CassiSystem()
```

Class that contains the optical system main attributes and methods

## __init__

```python
def __init__(system_config_path)
```

Load the system configuration file and initialize the grids for the DMD and the detector

**Arguments**:

 `system_config_path` _str_ : path to the system configuration file
  
**Initial Attributes**:
- `system_config` _dict_ - system configuration
- `X_dmd_coordinates_grid` _numpy array_ - X grid coordinates of the center of the DMD pixels
- `Y_dmd_coordinates_grid` _numpy array_ - Y grid coordinates of the center of the DMD pixels
- `X_detector_coordinates_grid` _numpy array_ - X grid coordinates of the center of the detector pixels
- `Y_detector_coordinates_grid` _numpy array_ - Y grid coordinates of the center of the detector pixels


## load dataset

```python
def load_dataset(directory, dataset_name)
```

Loading the dataset

**Arguments**:

- `directory` _str_ - name of the directory containing the dataset
- `dataset_name` _str_ - dataset name
  

**Returns**:

  list_dataset_data (list) : a list containing the dataset, the ground truth, the list of wavelengths, the label values, the ignored labels, the rgb bands, the palette and the delta lambda


## update\_config

```python
def update_config(new_config)
```

Update the system configuration and recalculate the DMD and detector grids coordinates

**Arguments**:

- `new_config` _dict_ - new system configuration
  

**Returns**:

  config (dict) : updated system configuration


## interpolate\_dataset\_along\_wavelengths

```python
def interpolate_dataset_along_wavelengths(new_wavelengths_sampling,
                                          chunk_size)
```

Interpolate the dataset cube along the wavelength axis to match the system sampling

**Arguments**:

- `new_wavelengths_sampling` _numpy array_ - system wavelengths sampling
- `chunk_size` _int_ - chunk size for the multiprocessing
  

**Returns**:

- `dataset_interpolated` _numpy array_ - interpolated dataset cube along the wavelength axis

## generate\_2D\_mask

```python
def generate_2D_mask(config_mask_and_filtering)
```

**Arguments**:

- `config_mask_and_filtering` _dict_ - masks and filtering configuration
  

**Returns**:

- `mask` _numpy array_ - 2D DMD mask based on the configuration file

## generate\_filtering\_cube

```python
def generate_filtering_cube()
```

Generate filtering cube, each slice is a propagated mask interpolated on the detector grid

**Returns**:

- `filtering_cube` _numpy array_ - 3D filtering cube generated according to the system configuration


## image\_acquisition

```python
def image_acquisition(use_psf=False, chunck_size=50)
```

Run the acquisition process depending on the cassi system type

**Arguments**:

- `chunck_size` _int_ - default block size for the dataset
  

**Returns**:

- `last_filtered_interpolated_scene` _numpy array_ - filtered scene cube
- `interpolated_scene` _numpy array_ - interpolated scene cube

## generate\_sd\_measurement\_cube

```python
def generate_sd_measurement_cube(scene)
```

Generate SD measurement cube from the scene cube and the filtering cube

**Arguments**:

- `scene` _numpy array_ - scene cube
  

**Returns**:

- `measurement_sd` _numpy array_ - SD measurement cube

## create\_coordinates\_grid

```python
def create_coordinates_grid(nb_of_samples_along_x, nb_of_samples_along_y,
                            delta_x, delta_y)
```

Create a coordinates grid for a given number of samples along x and y axis and a given pixel size

**Arguments**:

- `nb_of_samples_along_x` _int_ - number of samples along x axis
- `nb_of_samples_along_y` _int_ - number of samples along y axis
- `delta_x` _float_ - pixel size along x axis
- `delta_y` _float_ - pixel size along y axis
  

**Returns**:

- `X_input_grid` _numpy array_ - x coordinates grid
- `Y_input_grid` _numpy array_ - y coordinates grid

## calculate\_alpha\_c

```python
def calculate_alpha_c()
```

Calculate the relative angle of incidence between the lenses and the dispersive element

**Returns**:

- `alpha_c` _float_ - angle of incidence


## propagate\_mask\_grid

```python
def propagate_mask_grid(X_input_grid=None, Y_input_grid=None)
```

Propagate the SLM mask through one CASSI system

**Arguments**:

- `X_input_grid` _numpy array_ - x coordinates grid
- `Y_input_grid` _numpy array_ - y coordinates grid
  

**Returns**:

- `list_X_propagated_mask` _list_ - list of the X coordinates of the propagated masks
- `list_Y_propagated_mask` _list_ - list of the Y coordinates of the propagated masks

## generate\_psf

```python
def generate_psf(type, radius)
```

Generate a PSF

**Arguments**:

- `type` _str_ - type of PSF to generate
- `radius` _float_ - radius of the PSF
  

**Returns**:

- `PSF` _numpy array_ - PSF generated

## apply\_psf

```python
def apply_psf()
```

Apply the PSF to the last measurement

**Returns**:

- `last_filtered_interpolated_scene` _numpy array_ - last measurement convolved with by PSF. Each slice of the 3D filtered scene is convolved with the PSF

## save\_acquisition

```python
def save_acquisition(config_mask_and_filtering, config_acquisition)
```

Save the all data related to an acquisition

**Arguments**:

- `config_mask_and_filtering` _dict_ - configuration dictionary for the mask and filtering parameters
- `config_acquisition` _dict_ - configuration dictionary for the acquisition parameters

## worker

```python
def worker(args)
```

Process to parallellize

**Arguments**:

- `args`: 

